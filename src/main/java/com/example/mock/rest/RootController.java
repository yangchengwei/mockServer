package com.example.mock.rest;

import com.oracle.tools.packager.IOUtils;
import com.sun.corba.se.impl.protocol.giopmsgheaders.RequestMessage;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.thrift.TException;
import org.apache.thrift.TSerializer;
import org.apache.thrift.protocol.*;
import org.apache.thrift.transport.TMemoryBuffer;
import org.apache.thrift.transport.TTransportException;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;
import com.example.mock.thrift.*;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.filter.HiddenHttpMethodFilter;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;


/**
 * Created by trila on 2017/9/28.
 */
@RestController("/")
public class RootController {
    @Bean
    public FilterRegistrationBean registration(HiddenHttpMethodFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean(filter);
        registration.setEnabled(false);
        return registration;
    }

    static ArrayList<TSpan> cache = new ArrayList<>();

    @RequestMapping(method = RequestMethod.POST)
    public String post(HttpServletRequest request, HttpServletResponse response){
        String type = request.getHeader("Kepler-Message-Type");
        String result = "";

        switch (type) {
            case "Agent":
                result = agent(request);
                break;
            case "SPAN_BATCH":
                result = spanBatch(request);
                break;
            case "HEALTHCHECK":
                result = healthCheck(request);
                break;
        }

        response.setHeader("Kepler-Message-Type", type);

        return result;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String get(HttpServletRequest request, HttpServletResponse response){
        String result = "[";

        for (int i = 0; i < cache.size(); i++) {
            TSpan tSpan = cache.get(i);
            byte[] body = new byte[500];
            TMemoryBuffer tmb = new TMemoryBuffer(500);
            try {
                tmb.write(body);
            } catch (TTransportException e) {
                e.printStackTrace();
            }
            TSerializer serializer = new TSerializer(new TSimpleJSONProtocol.Factory());
            try {
                result += serializer.toString(tSpan);
                result += ",";
            } catch (TException e) {
                e.printStackTrace();
            }
        }

        return result.substring(0, result.length() - 1) + "]";
    }

    private String agent(HttpServletRequest request) {
        TAgentInfo agentInfo = new TAgentInfo();

        try {
            Integer length = Integer.valueOf(request.getHeader("Content-Length"));
            byte[] body = new byte[length];
            request.getInputStream().read(body);
            TMemoryBuffer tmb = new TMemoryBuffer(length);
            tmb.write(body);
            TCompactProtocol proto = new TCompactProtocol(tmb);
            agentInfo.read(proto);
//            System.out.println(agentInfo);
        } catch (TException e) {
            e.printStackTrace();
        } catch (IOException e) {
//            e.printStackTrace();
        } finally {
//            System.out.println(request.getHeader("Kepler-Agent-Token"));
//            System.out.println(request.getHeader("Kepler-Agent-Fingerprint"));
        }
        return "";
    }

    private String healthCheck(HttpServletRequest request) {
        TAgentHealthcheckRequest agentHealthcheckRequest = new TAgentHealthcheckRequest();

        try {
            Integer length = Integer.valueOf(request.getHeader("Content-Length"));
            byte[] body = new byte[length];
            request.getInputStream().read(body);
            TMemoryBuffer tmb = new TMemoryBuffer(length);
            tmb.write(body);
            TCompactProtocol proto = new TCompactProtocol(tmb);
            agentHealthcheckRequest.read(proto);
            //System.out.println(agentHealthcheckRequest);
        } catch (TException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
//            System.out.println(request.getHeader("Kepler-Agent-Token"));
//            System.out.println(request.getHeader("Kepler-Agent-Fingerprint"));
        }

        TAgentHealthcheckResult agentHealthcheckResult = new TAgentHealthcheckResult();

        agentHealthcheckResult.setStatus(TAgentStatus.OK);
        return "";
    }

    private String spanBatch(HttpServletRequest request) {
        this.cache.clear();
        TSpanBatch spanBatch = new TSpanBatch();

        try {
            Integer length = Integer.valueOf(request.getHeader("Content-Length"));
            byte[] body = new byte[length];
            request.getInputStream().read(body);
            TMemoryBuffer tmb = new TMemoryBuffer(length);
            tmb.write(body);
            TCompactProtocol proto = new TCompactProtocol(tmb);
            spanBatch.read(proto);
            System.out.println(spanBatch);
            cache.addAll(spanBatch.getSpans());
        } catch (TException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            System.out.println(request.getHeader("Kepler-Agent-Token"));
            System.out.println(request.getHeader("Kepler-Agent-Fingerprint"));
        }
        return "";
    }
}
